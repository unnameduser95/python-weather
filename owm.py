from pyowm import OWM
import json
import sys

jsonData = {}

with open("api_key.txt", "r") as key_file:
  API_KEY = key_file.read()
  owm = OWM(API_KEY)  # register free API key

UNIT = 'fahrenheit'  # supported units: fahrenheit, celsius, kelvin

with open("city.list.json", "r") as cityFile:
  jsonData = json.loads(cityFile.read())  # load list of cities from JSON file

def search_cities(cityName, countryCode = None):  # perform search for cities
  cities = []
  if (countryCode):
    for city in jsonData:
      if (city['name'].lower() == cityName.lower() and city['country'].lower() == countryCode.lower()): cities.append(city)
  else:
    for city in jsonData:
      if (city['name'].lower() == cityName.lower()): cities.append(city)
    
  return cities  # returns list of dictionaries

def get_temperature(cityID, unit = 'kelvin'):  # get temperature
  obs = owm.weather_at_id(cityID)

  w = obs.get_weather()
  return w.get_temperature(unit)  # returns dict

def get_UV_value(latitude, longitude):
  uvi = owm.uvindex_around_coords(latitude, longitude)
  return uvi.get_value()  # returns float

def get_UV_rating(uvi):
  if (uvi < 3.0):
    return 'low'
  elif (uvi < 6.0):
    return 'moderate'
  elif (uvi < 8.0):
    return 'high'
  elif (uvi < 10.0):
    return 'very high'
  elif (uvi >= 10.0):
    return 'extreme'

while (__name__ == "__main__"):  # runs if main program
  while True:
    searchQuery = str(input("Enter a city: "))
    if (searchQuery == "quit"):
      sys.exit(0)

    searchCity = searchQuery.split(', ')
    # print(searchCity)
    # print(tuple(searchCity))

    searchResults = []  # get search result
    if (len(searchCity) == 2):
      searchResults = search_cities(searchCity[0], searchCity[1])
    elif (len(searchCity) == 1):
      searchResults = search_cities(searchCity[0])
    # print(searchResults)

    if (searchResults == []):  # if no search results
      print('There were no results for "%s". Please try again with a different query.' % searchQuery)
      break

    count = 1
    for item in searchResults:
      print("%s. %s, %s @ latitude %s, longitude %s" % (count, item['name'], item['country'], item['coord']['lat'], item['coord']['lon']))
      count += 1

    citySelection = int(input("Pick a city #: "))  # select city from list
    selectedCity = searchResults[citySelection - 1]  # selected city is recorded

    # print(selectedCity)

    temperature = get_temperature(selectedCity['id'], UNIT)  # get temperatures
    uvi = get_UV_value(selectedCity['coord']['lat'], selectedCity['coord']['lon'])  # get UVI
    uv_rating = get_UV_rating(uvi)

    # print(temperature)
    # print(uvi)

    print("The current temperature of %s, %s is %s° %s. The maximum temperature is %s°, and the minimum temperature is %s°. The UV index is %s, which puts people at a %s risk of harm." % (selectedCity['name'], selectedCity['country'], temperature['temp'], UNIT, temperature['temp_max'], temperature['temp_min'], uvi, uv_rating))
    
