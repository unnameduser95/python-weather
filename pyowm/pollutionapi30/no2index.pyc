ó
Đp"^c           @   sp   d  Z  d d l Z d d l j j Z d d l m Z m Z d d l	 m
 Z
 m Z m Z d e f d     YZ d S(   s/   
Nitrogen Dioxide classes and data structures.
i˙˙˙˙N(   t   NO2INDEX_XMLNS_URLt   NO2INDEX_XMLNS_PREFIX(   t   timeformatutilst	   timeutilst   xmlutilst   NO2Indexc           B   s   e  Z d  Z d   Z d d  Z d d  Z d   Z d   Z d   Z d   Z	 d	   Z
 d
   Z e e d  Z d   Z d   Z RS(   sÄ  
    A class representing the Nitrogen DiOxide Index observed in a certain location
    in the world. The index is made up of several measurements, each one at a
    different atmospheric levels. The location is represented by the 
    encapsulated *Location* object.

    :param reference_time: GMT UNIXtime telling when the NO2 data has been measured
    :type reference_time: int
    :param location: the *Location* relative to this NO2 observation
    :type location: *Location*
    :param interval: the time granularity of the NO2 observation
    :type interval: str
    :param no2_samples: the NO2 samples
    :type no2_samples: list of dicts
    :param reception_time: GMT UNIXtime telling when the NO2 observation has
        been received from the OWM web API
    :type reception_time: int
    :returns: a *NO2Index* instance
    :raises: *ValueError* when negative values are provided as reception time,
      NO2 samples are not provided in a list

    c         C   s   | d k  r t  d   n  | |  _ | |  _ | |  _ t | t  sT t  d   n  | |  _ | d k  rx t  d   n  | |  _ d  S(   Ni    s'   'reference_time' must be greater than 0s   'no2_samples' must be a lists'   'reception_time' must be greater than 0(   t
   ValueErrort   _reference_timet	   _locationt	   _intervalt
   isinstancet   listt   _no2_samplest   _reception_time(   t   selft   reference_timet   locationt   intervalt   no2_samplest   reception_time(    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyt   __init__#   s    				t   unixc         C   s   t  j |  j |  S(   sÝ  
        Returns the GMT time telling when the NO2 samples have been measured

        :param timeformat: the format for the time value. May be:
            '*unix*' (default) for UNIX time
            '*iso*' for ISO8601-formatted string in the format ``YYYY-MM-DD HH:MM:SS+00``
            '*date* for ``datetime.datetime`` object instance
        :type timeformat: str
        :returns: an int or a str
        :raises: ValueError when negative values are provided

        (   R   t
   timeformatR   (   R   R   (    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyt   get_reference_time1   s    c         C   s   t  j |  j |  S(   sý  
        Returns the GMT time telling when the NO2 observation has been received
        from the OWM web API

        :param timeformat: the format for the time value. May be:
            '*unix*' (default) for UNIX time
            '*iso*' for ISO8601-formatted string in the format ``YYYY-MM-DD HH:MM:SS+00``
            '*date* for ``datetime.datetime`` object instance
        :type timeformat: str
        :returns: an int or a str
        :raises: ValueError when negative values are provided

        (   R   R   R   (   R   R   (    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyt   get_reception_time@   s    c         C   s   |  j  S(   sx   
        Returns the *Location* object for this NO2 index measurement

        :returns: the *Location* object

        (   R   (   R   (    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyt   get_locationP   s    c         C   s   |  j  S(   sl   
        Returns the time granularity interval for this NO2 index measurement

        :return: str
        (   R	   (   R   (    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyt   get_intervalY   s    c         C   s   |  j  S(   sZ   
        Returns the NO2 samples for this index

        :returns: list of dicts

        (   R   (   R   (    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyt   get_no2_samplesa   s    c         C   s,   x% |  j  D] } | d | k r
 | Sq
 Wd S(   sĹ   
        Returns the NO2 sample having the specified label or `None` if none 
        is found

        :param label: the label for the seeked NO2 sample 
        :returns: dict or `None`

        t   labelN(   R   t   None(   R   R   t   sample(    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyt   get_sample_by_labelj   s    	c         C   s"   t  j d d  |  j d d  k  S(   s   
        Tells if the current NO2 observation refers to the future with respect
        to the current date
        :return: bool
        R   R   (   R   t   nowR   (   R   (    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyt   is_forecastx   s    c         C   sN   t  j i |  j d 6t  j |  j j    d 6|  j d 6|  j d 6|  j d 6 S(   s^   Dumps object fields into a JSON formatted string

        :returns:  the JSON string

        R   R   R   R   R   (	   t   jsont   dumpsR   t   loadsR   t   to_JSONR	   R   R   (   R   (    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyR%      s
    

c         C   s8   |  j    } | r( t j | t t  n  t j | |  S(   s&  
        Dumps object fields to an XML-formatted string. The 'xml_declaration'
        switch  enables printing of a leading standard XML line containing XML
        version and encoding. The 'xmlns' switch enables printing of qualified
        XMLNS prefixes.

        :param XML_declaration: if ``True`` (default) prints a leading XML
            declaration line
        :type XML_declaration: bool
        :param xmlns: if ``True`` (default) prints full XMLNS prefixes
        :type xmlns: bool
        :returns: an XML-formatted string

        (   t   _to_DOMR   t   annotate_with_XMLNSR   R    t   DOM_node_to_XML(   R   t   xml_declarationt   xmlnst	   root_node(    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyt   to_XML   s    	
c         C   s  t  j d  } t  j | d  } t |  j  | _ t  j | d  } t |  j  | _ t  j | d  } t |  j  | _ t  j | d  } xl |  j D]a } | j	   } | d | d <d j
 | d  | d <d j
 | d	  | d	 <t j | d
 |  q W| j |  j j    | S(   s   
        Dumps object data to a fully traversable DOM representation of the
        object.

        :returns: a ``xml.etree.Element`` object

        t   no2indexR   R   R   R   R   s   {:.12e}t   valuet	   precisiont
   no2_sample(   t   ETt   Elementt
   SubElementt   strR   t   textR   R	   R   t   copyt   formatR   t   create_DOM_node_from_dictt   appendR   R&   (   R   R+   t   reference_time_nodet   reception_time_nodet   interval_nodet   no2_samples_nodet   smplt   s(    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyR&   ¤   s"    c         C   s>   d t  |  j j  |  j d  |  j d  t |  j  |  j f S(   NsH   <%s.%s - reference time=%s, reception time=%s, location=%s, interval=%s>t   iso(   t   __name__t	   __class__R   R   R4   R   R	   (   R   (    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyt   __repr__ż   s    	(   RA   t
   __module__t   __doc__R   R   R   R   R   R   R   R!   R%   t   TrueR,   R&   RC   (    (    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyR      s   											(   RE   R"   t   xml.etree.ElementTreet   etreet   ElementTreeR1   t$   pyowm.pollutionapi30.xsd.xmlnsconfigR    R   t   pyowm.utilsR   R   R   t   objectR   (    (    (    sp   /private/var/folders/d6/d10sy_853xj7srymh4ks51th0000gn/T/pip-build-9jrfdE/pyowm/pyowm/pollutionapi30/no2index.pyt   <module>   s
   